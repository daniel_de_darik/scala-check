package zalando.lisa.number

import scala.collection.mutable.ArrayBuffer

/**
  * Created by darik on 7/3/16.
  */
object Converter {
  def toBase(x: Int, base: Int): List[Int] = {
    val list = ArrayBuffer[Int]()
    var num: Int = x
    while (num > 0) {
      val rem = num % base
      list += rem
      num /= base
    }

    list.reverse.toList
  }

  def fromBase(num: List[Int], base: Int): Int = {
    var ret = 0
    for(i <- num) ret = ret * base + i
    ret
  }
}
