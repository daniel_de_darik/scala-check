package zalando.lisa.model

/**
  * Created by darik on 7/4/16.
  */
case class Commission(id: Int, number: String, items: List[Int])
