package zalando.lisa.search

import scala.annotation.tailrec

/**
  * Created by darik on 7/3/16.
  */
object Linear {
  def search(arr: Array[Int], needle: Int): Int = {
    find(0, arr, needle)
  }

  @tailrec
  def find(pos: Int, arr: Array[Int], needle: Int): Int = {
    if (pos == arr.length) -1
    else if (arr(pos) == needle) pos
    else find(pos + 1, arr, needle)
  }
}
