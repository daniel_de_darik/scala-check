package zalando.lisa.search

/**
  * Created by darik on 7/3/16.
  */
object Binary {
  def search(arr: Array[Int], needle: Int): Int = {
    var lo: Int = -1
    var hi: Int = arr.length

    while(hi - lo > 1) {
      val mid = lo + (hi - lo) / 2
      if (arr(mid) < needle) {
        lo = mid
      } else {
        hi = mid
      }
    }

    if (hi == arr.length || arr(hi) != needle) -1
    else hi
  }
}
