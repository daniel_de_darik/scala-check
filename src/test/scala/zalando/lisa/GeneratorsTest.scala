package zalando.lisa

import org.scalacheck.{Gen, Prop, Properties}
import org.scalacheck.Prop._

/**
  * Created by darik on 7/3/16.
  */
class GeneratorsTest extends Properties("Generators test suite"){

  property(" Random numbers") = Prop.forAll{
    (x: Int, y: Int, z: Int) => {
      (x + y) + z == x + (y + z)
    }
  }

  property(" Should generate only one value") = Prop.forAll(Gen.const("scala-check")) {
    (x: String) => {
      x == "scala-check"
    }
  }

  property(" Should generate random numbers") = Prop.forAll(Gen.choose(-10000, 10000), Gen.choose(-1, 1)) {
    (x: Int, y: Int) => {
      -10000 <= x && x <= 10000 && -1 <= y && y <= 1
    }
  }

  property(" Should generate random strings") = Prop.forAll(Gen.alphaStr) {
    (x: String) => {
      (x != "") ==> {
        x.length > 0
      }
    }
  }

  property(" Should generate random list") = Prop.forAll(Gen.listOf(Gen.choose(-1000, 1000))) {
    (l: List[Int]) => {
      (l.size > 0) ==> {
        l.nonEmpty
      }
    }
  }

  property(" Should generate random list of specified size") = Prop.forAll(Gen.listOfN(3, Gen.alphaChar)) {
    (l: List[Char]) => {
      l.size == 3
    }
  }

  property(" Should take one of values") = Prop.forAll(Gen.oneOf(Gen.choose(-10, 10), Gen.alphaStr)) {
    _ match {
      case x: Int => -10 <= x && x <= 10
      case s: String => s.length >= 0
    }
  }

  property(" Should generate numbers with frequency and show distribution of data") = Prop.forAll(Gen.choose(-100, 100)) {
    (x: Int) => {
      classify(-100 <= x && x <= -50, "[-100, -50]") {
        classify(-50 < x && x <= 0, "(-50, 0]") {
          classify(0 < x & x <= 50, "(0, 50]") {
            classify(50 < x && x <= 100, "(50, 100]") {
              true
            }
          }
        }
      }
    }
  }

  property(" Should generate data with configured data distribution") = Prop.forAll(Gen.frequency(
    (1, Gen.const(1)),
    (3, Gen.const(2)),
    (2, Gen.const(3))
  )) {
    (x: Int) => {
      classify(x == 1, "x = 1") {
        classify(x == 2, "x = 2") {
          classify(x == 3, "x = 3") {
            true
          }
        }
      }
    }
  }

//  property(" Should not show which assertion has failed") = Prop.forAll(Gen.posNum[Int], Gen.posNum[Int]) {
//    (x: Int, y: Int) => {
//      (x + y == y + x) && (x + y > 0) && (x + y < y + x)
//    }
//  }

  property(" Should show which assertion has failed") = Prop.forAll(Gen.posNum[Int], Gen.posNum[Int]) {
    (x: Int, y: Int) => {
      (x + y == y + x) :| "x + y == y + x" &&
      (x + y > 0)      :| "x + y > 0" &&
      (x + y <= x + y) :| "x + y < x + y"
    }
  }
}
