package zalando.lisa

import org.scalacheck.{Arbitrary, Gen, Prop, Properties}
import org.scalacheck.Prop._
import zalando.lisa.model.Commission

/**
  * Created by darik on 7/4/16.
  */

object PrimeNumberGenerator {
  implicit val number: Arbitrary[Int] = Arbitrary {
    for {
      i <- Gen.posNum[Int] if i > 1 && isPrime(i)
    } yield i
  }

  def isPrime(x: Int): Boolean = (2 to Math.sqrt(x).toInt + 1).forall(x % _ != 0)
}

class PrimeNumberGenerator extends Properties("Arbitrary test suite for prime numbers") {

  import PrimeNumberGenerator._

  property(" Make sure that all prime numbers positive") = Prop.forAll {
    (x: Int) => {
      classify(x < 10, "[2, 10)") {
        classify(x >= 10 && x < 100, "[10, 100)") {
          classify(x >= 100 && x < 500, "[100, 500)") {
            classify(x >= 500, "[500, oo)") {
              x > 0
            }
          }
        }
      }
    }
  }
}

object CommissionGenerator {
  implicit val commission: Arbitrary[Commission] = Arbitrary {
    for {
      id <- Gen.posNum[Int]
      number <- nonEmptyStringGen
      items <- nonEmptyListGen
    } yield Commission(id, number, items)
  }

  val nonEmptyStringGen = for {
    s <- Gen.alphaStr if (s.length > 0)
  } yield s

  val nonEmptyListGen = for {
    list <- Gen.listOf(Gen.posNum[Int]) if (list.size > 0)
  } yield list
}

class ArbitraryTest extends Properties("Arbitrary test suite") {

  import CommissionGenerator._

  property(" Make sure commissin id is positive") = Prop.forAll {
    (commission: Commission) => {
      collect((commission.id, commission.number)) {
        commission.id > 0
      }
    }
  }

  property(" Make sure commisson number not empty") = Prop.forAll {
    (commission: Commission) => {
      !commission.number.isEmpty
    }
  }

  property(" Make sure commission has at least on commission item") = Prop.forAll {
    (commission: Commission) => {
      commission.items.nonEmpty
    }
  }
}
