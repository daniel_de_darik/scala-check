package zalando.lisa

import org.scalacheck.{Gen, Prop, Properties}

/**
  * Created by darik on 7/3/16.
  */
class SimpleMathTest extends Properties("Simple math test suite") {
  // Left side is property specifier, Right side is Prop test.
  property(" Make sure sum is commutative in Scala") = Prop.forAll(Gen.posNum[Int], Gen.posNum[Int]) {
    (a: Int, b: Int) => {
      a + b == b + a
    }
  }

  property(" Make sure sum is associative") = Prop.forAll {
    (a: Int, b: Int, c: Int) => {
      (a + b) + c == a + (b + c)
    }
  }

  property(" Make sure distributive laws works as well") = Prop.forAll {
    (a: Int, b: Int, c: Int) => {
      a * (b + c) == a * b + a * c
    }
  }

  property(" Make sure arithmetics works in Scala") = Prop.forAll {
    (x: Int) => {
      x > x - 1
    }
  }
}
