package zalando.lisa

import org.specs2.matcher.DataTables
import org.specs2.mutable.Specification

/**
  * Created by dimangaliyev on 05/07/16.
  */
class Specs2DataTableTest extends Specification with DataTables {
  override def is =
    sequential ^
  s2"""
  Generate data with DataTables $dataTableTest
  """

  def dataTableTest =
    "First argument" | "Second argument" | "Third argument" | "Expected" |
    -1               ! -3                ! -2               ! -6         |
    1                ! 2                 ! 3                ! 6          |> {
      (a: Int, b: Int, c: Int, d: Int) => {
//        println(s"$a + $b + $c = $d")
        (a + b + c) must beEqualTo(d)
      }
    }
}
