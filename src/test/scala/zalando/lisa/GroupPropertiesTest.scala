package zalando.lisa

import org.scalacheck.{Gen, Prop, Properties}
import org.scalacheck.Prop._

/**
  * Created by darik on 7/3/16.
  */
class GroupPropertiesTest extends Properties("Combine properties test suite"){

  val stringOnly = Prop.forAll(Gen.alphaStr) {
    (x: String) => {
      (x != "") ==> {
        x.nonEmpty
      }
    }
  }

  val positiveNumbersOnly = Prop.forAll(Gen.posNum[Int]) {
    (x: Int) => x > 0
  }

  val buggyPositiveNumbersOnly = Prop.forAll(Gen.negNum[Int]) {
    (x: Int) => x >= 0
  }

  val alwaysPass = Prop.forAll {
    (x: Int) => true
  }

  val wontPass = Prop.forAll(Gen.posNum[Int]) {
    (x: Int) => x % 2 == 0
  }

  val evenTest = Prop.forAll(Gen.posNum[Int]) {
    (x: Int) => {
      x % 2 == 0
    }
  }

  val oddTest = Prop.forAll(Gen.posNum[Int]) {
    (x: Int) => {
      x % 2 == 1
    }
  }

  property(" All number whether even or odd") = evenTest || oddTest

//  property(" Test that always passes") = alwaysPass || wontPass
//  property("Test that never passes") = wontPass && alwaysPass
//  property(" Non empty strings have positive length && always pass") = stringOnly && alwaysPass
//  property(" 0 is not positive number, should fail") = buggyPositiveNumbersOnly && alwaysPass
}
