package zalando.lisa

import org.scalacheck.{Gen, Prop, Properties}
import zalando.lisa.search.{Binary, Linear}
import org.scalacheck.Prop._
import zalando.lisa.number.Converter

/**
  * Created by darik on 7/3/16.
  */

class SearchTest extends Properties("Search test suite"){
  // Model based testing
  property(" Model based testing") = Prop.forAll(Gen.listOf(Gen.choose(-100000, 10000)), Gen.choose(-100000, 100000)) {
    (list: List[Int], needle: Int) => {
      val arr = list.sortWith((x, y) => x <= y).toArray[Int]
      Binary.search(arr, needle) == Linear.search(arr, needle)
    }
  }

  // Symmetry based testing
  property(" Symmetry based testing") = Prop.forAll(Gen.choose(1, 1000000), Gen.choose(2, 10)){
    (x: Int, base: Int) => {
        val numberInDifferentBase = Converter.toBase(x, base)
        Converter.fromBase(numberInDifferentBase, base) == x
    }
  }
}
