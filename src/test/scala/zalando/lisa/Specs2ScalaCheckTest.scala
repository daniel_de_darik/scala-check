package zalando.lisa

import org.scalacheck.{Gen, Prop}
import org.specs2.ScalaCheck
import org.specs2.mutable._

/**
  * Created by dimangaliyev on 05/07/16.
  */
class Specs2ScalaCheckTest extends Specification with ScalaCheck {

  override def is =
    sequential ^
  s2"""
     "Using scala check with specs2"
        "Make sure formula for calculating sum of numbers correct"  $checkTheSumFromA2B
        "Make sure that concatenating works" $checkListConcatenation
    """

  def checkTheSumFromA2B = Prop.forAll(Gen.choose(1, 100), Gen.choose(101, 200)) {
    (a: Int, b: Int) => {
      println(a + " " + b)
      (a to b).sum must beEqualTo((a + b) * (b - a + 1) / 2)
    }
  }

  def checkListConcatenation = Prop.forAll {
    (a: List[Int], b: List[Int]) => {
      a.size + b.size must beEqualTo((a ::: b).size)
    }
  }
}
