#!/usr/bin/env bash

javac gen.java
scalac SmartSearch.scala
scalac NaiveSearch.scala

DIFF=0
COUNT=0
while [ $DIFF -ne 1 ]
do
	java gen > input.txt
	scala SmartSearch < input.txt > out1.txt
	scala NaiveSearch < input.txt > out2.txt

	if diff out1.txt out2.txt > /dev/null; then
		echo "CORRECT $COUNT"
	else
		echo "WRONG ANSWER $COUNT"
		let "DIFF=1"
	fi
	let "COUNT+=1"
done