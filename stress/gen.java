import java.util.*;

public class gen {

    final String ALPHABET = "abcdefghigklmnopqrstuvwxyz";
    final Random rnd = new Random();

	public void run() {

        int len = rnd.nextInt(3) + 1;
        System.out.println(getRandomString(len));
        len = rnd.nextInt(100) + 2;
        System.out.println(getRandomString(len));
	}

    public String getRandomString(int len) {
        StringBuilder sb = new StringBuilder();
        while(sb.length() < len) {
            sb.append(ALPHABET.charAt(rnd.nextInt(ALPHABET.length())));
        }
        return sb.toString();
    }

	public static void main(String []args) {
		new gen().run();
	}
}
