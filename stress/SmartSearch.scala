object SmartSearch {
  def main(arr: Array[String]): Unit = {
    val pattern = readLine().toString + "$"
    val text = readLine().toString

    val p = Array.fill[Int](pattern.length)(0)
    for (i <- 1 until pattern.length) {
      var j = p(i - 1)
      while(j > 0 && pattern(i) != pattern(j)) {
        j = p(j - 1)
      }
      if (pattern(i) == pattern(j)) j += 1
      p(i) = j
    }

    var prev = 0
    for(i <- 0 until text.length) {
      var j = prev
      while(j > 0 && text(i) != pattern(j)) {
        j = p(j - 1)
      }
      if (text(i) == pattern(j)) j += 1
      if (j == pattern.length - 1) {
        println(i - pattern.length + 2)
      }
      prev = j
    }
  }
}