object NaiveSearch {
  def main(args: Array[String]): Unit = {
    val pattern = readLine().toString
    val text = readLine().toString

    for(i <- 0 to text.length - pattern.length) {
      var found = true
      for (j <- 0 until pattern.length) {
        found &= text(i + j) == pattern(j)
      }
      if (found) {
        println(i)
      }
    }
  }
}