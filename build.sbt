name := "Testing in Scala. ScalaCheck"

version := "1.0"

scalaVersion := "2.11.6"

resolvers ++= Seq(
  "snapshots" at "http://oss.sonatype.org/content/repositories/snapshots",
  "releases"  at "http://oss.sonatype.org/content/repositories/releases"
)

libraryDependencies ++= Seq(
  "org.specs2" %% "specs2" % "3.7",
//  "org.scalacheck" %% "scalacheck" % "1.13.0" % "test"
  "org.scalacheck" %% "scalacheck" % "1.12.5" % "test" // to make it compitable with specs2
)
